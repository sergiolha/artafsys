import React from 'react';
import { NavLink } from 'react-router-dom';
import { useAuth } from '../context/auth';

// Component who handles the menu section
function Menu() {
  const { authTokens } = useAuth();
  const currentUser = authTokens;

  const AdminMenu = () => {
    return(
      <React.Fragment>
        <li className="nav-item">
          <NavLink to="/dashboard" exact activeClassName="active" className="nav-link">
            <span data-feather="home"></span> Dashboard
          </NavLink>
        </li>
        { currentUser.profileId === 1 ?
        <li className="nav-item">
          <NavLink to="/dashboard/users" exact activeClassName="active" className="nav-link">
            <span data-feather="users"></span> Users
          </NavLink>
        </li> : null }
        { currentUser.profileId === 2 ?
        <li className="nav-item">
          <NavLink to="/dashboard/tickets" exact activeClassName="active" className="nav-link">
            <span data-feather="approvals"></span> Approvals
          </NavLink>
        </li> : null }
        { currentUser.profileId === 3 ?
        <li className="nav-item">
          <NavLink to="/dashboard/tickets" exact activeClassName="active" className="nav-link">
            <span data-feather="tickets"></span> Tickets
          </NavLink>
        </li> : null }
      </React.Fragment>
    );
  }

  return(
    <nav className="col-md-2 d-none d-md-block bg-light sidebar">
      <div className="sidebar-sticky">
        <ul className="nav flex-column">
          <AdminMenu />
        </ul>
      </div>
    </nav>
  );
}

export default Menu;