import React from 'react';
import { Navbar, NavbarBrand, Nav, NavItem } from 'reactstrap';
import { useAuth } from "../context/auth";

// Component who handles the head section
function Header(props) {
  const { setAuthTokens } = useAuth();

  function logOut() {
    setAuthTokens('');
  }

  return(
    <React.Fragment>
      <Navbar color="dark" className="navbar-dark sticky-top flex-md-nowrap p-0">
        <NavbarBrand href="#" className="col-sm-3 col-md-2 mr-0">Artaf System</NavbarBrand>
        <Nav className="px-3 navbar-nav">
          <NavItem className="text-nowrap">
            <button className="btn nav-link" onClick={logOut}>Sign Out</button>
          </NavItem>
        </Nav>
      </Navbar>
    </React.Fragment>
  );
}

export default Header;