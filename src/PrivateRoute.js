import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useAuth } from './context/auth';

function PrivateRoute({ component: Component, roles, ...rest }) {
  const { authTokens } = useAuth();
  
  return(
    <Route
      {...rest}
      render={(props) => {
        const user = authTokens;
        if (!user) {
          return <Redirect to={{pathname: '/auth/login', state: {from: props.location}}} />;
        }
        if (roles && roles.indexOf(user.profileId) === -1) {
          return <Redirect to={{pathname: '/dashboard'}} />;
        }
        return <Component {...props} {...rest} />;
      }}
    />
  );
}

export default PrivateRoute;