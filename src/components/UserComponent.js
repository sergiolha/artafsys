import React from 'react';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import UserForm from './UserFormComponent';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

/**
 * Users component class
 * 
 * The component handles listing, adding, editing and deleting of users
 */
class Users extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isUserFormOpen: false,
      user: null
    }
    this.selectUser = this.selectUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.toggleUserForm = this.toggleUserForm.bind(this);
    this.handleAddUser = this.handleAddUser.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  selectUser(user) {
    this.setState({user: user, isUserFormOpen: true});
  }

  toggleUserForm() {
    this.setState({isUserFormOpen: !this.state.isUserFormOpen});
  }

  handleSubmit(values, id) {
    this.toggleUserForm();
    if (id === null) {
      this.props.postUser(parseInt(values.profile), values.name, values.email, values.password);
    }
    else {
      this.props.patchUser(id, parseInt(values.profile), values.name, values.email, values.password);
    }
  }

  handleAddUser() {
    this.toggleUserForm();
    this.selectUser(null);
  }

  deleteUser(user) {
    var res = window.confirm('¿Are you sure?');
    if (res) { this.props.delUser(user.id); }
  }

  render() {
    if (this.props.users.isLoading) {
      return(<Loading />);
    }
    else if (this.props.users.errMess) {
      return(<h4>{this.props.users.errMess}</h4>);
    }
    else {
      return(
        <div>
          <Button type="button" className="btn-primary" onClick={this.handleAddUser}>
            <span className="fa fa-pencil fa-lg"></span> Add User
          </Button>
          <br/>
          <br/>
          <div className="table-responsive">
            <table className="table table-striped table-sm">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {this.props.users.data.map((user) => {
                  return (
                    <tr key={user.id}>
                        <td><Link to={`/dashboard/users/${user.id}`}>{user.id}</Link></td>
                        <td>{user.name}</td>
                        <td>{user.email}</td>
                        <td>
                          <button className="btn btn-secondary" onClick={() => {this.selectUser(user)}}>Edit</button>&nbsp;
                          { user.profileId !== 1 ? <button className="btn btn-danger" onClick={() => {this.deleteUser(user)}}>Delete</button> : null }
                        </td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
          <Modal isOpen={this.state.isUserFormOpen} toggle={this.toggleUserForm}>
            <ModalHeader toggle={this.toggleUserForm}>{this.state.user ? 'Edit User' : 'Add User' }</ModalHeader>
            <ModalBody>
              <UserForm postUser={this.props.postUser} profiles={this.props.profiles} user={this.state.user} handleSubmit={this.handleSubmit} />
            </ModalBody>
          </Modal>
        </div>
      );
    }
  }
}

export default Users;