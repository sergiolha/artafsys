import React from 'react';

// Component to show dashboard (home)
function Dashboard(props) {
  return(
    <React.Fragment>
      <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1>Dashboard</h1>
      </div>
    </React.Fragment>
  );
}

export default Dashboard;