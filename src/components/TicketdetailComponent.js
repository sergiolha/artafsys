import React from 'react';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';

/**
 * Ticketdetail component class
 * 
 * The component handles showing info for specific ticket
 */
class Ticketdetail extends React.Component {
  render() {
    if (this.props.isLoading) {
      return(<Loading />);
    }
    else if (this.props.errMess) {
      return(
        <div>
          <h4>{this.props.errMess}</h4>
        </div>
      );
    }
    else {
      return (
        <div>
          <p>
            <Link to="/dashboard/tickets" className="btn btn-secondary">Back to List</Link>
          </p>
          <h3>Ticket Detail</h3>
          <strong>ID:</strong> {this.props.ticket.id} <br/>
          <strong>Title:</strong> {this.props.ticket.title} <br/>
          <strong>Description:</strong> {this.props.ticket.description} <br/>
          <strong>Status:</strong> {this.props.ticket.status} <br/>
        </div>
      );
    }
  }
}

export default Ticketdetail;