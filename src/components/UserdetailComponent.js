import React from 'react';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';

/**
 * Userdetail component class
 * 
 * The component handles showing info for specific user
 */
class Userdetail extends React.Component {
  render() {
    if (this.props.isLoading) {
      return(<Loading />);
    }
    else if (this.props.errMess) {
      return(
        <div>
          <h4>{this.props.errMess}</h4>
        </div>
      );
    }
    else {
      return (
        <div>
          <p>
            <Link to="/dashboard/users" className="btn btn-secondary">Back to List</Link>
          </p>
          <h3>User Detail</h3>
          <strong>ID:</strong> {this.props.user.id} <br/>
          <strong>Name:</strong> {this.props.user.name} <br/>
          <strong>Email:</strong> {this.props.user.email} <br/>
          <strong>Profile:</strong> {this.props.profile.name} <br/>
        </div>
      );
    }
  }
}

export default Userdetail;