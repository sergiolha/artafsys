import React, { useState } from 'react';
import { Label, Button } from 'reactstrap';
import { LocalForm, Errors, Control } from 'react-redux-form';
import { Redirect } from 'react-router-dom';
import { useAuth } from "../context/auth";
import { baseUrl } from '../shared/baseUrl';

// Component to handle login page form
function Login(props) {
  const referer = props.location.state.from || '/dashboard';

  const required = (val) => val && val.length;
  const validEmail = (val) => /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/i.test(val);
  
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [isError, setIsError] = useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { setAuthTokens } = useAuth();

  function handleSubmit(values) {
    setEmail(values.email);
    setPassword(values.password);

    return fetch(baseUrl + 'users')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var err = new Error('Error ' + response.status + ': ' + response.statusText);
                err.response = response;
                throw err;
            }
        },
            error => {
                var errm = new Error(error.message);
                throw errm;
            })
        .then(response => response.json(response))
        .then(response => {
          var Users = response;
          const userFound = Users.filter((user) => (user.email === values.email && user.password === values.password))[0];
          if (!userFound) {
            setIsError(true);
          }
          else {
            setIsError(false);
            setAuthTokens({
              userId: userFound.id,
              username: userFound.name,
              profileId: userFound.profileId,
              key:'YB45BN39OP'
            });
            setLoggedIn(true);
          }
        })
        .catch(err => alert(err.message));
  }

  if (isLoggedIn === true) {
    return <Redirect to={referer} />;
  }

  return(
    <div className="login">
    <div className="text-center">
      <LocalForm className="form-signin" onSubmit={(values) => handleSubmit(values)}>
        <img className="mb-4" src="" alt="" />
        <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
        { isError ? <div className="text-danger">Incorrect email and/or password</div> : null}
        <Label htmlFor="email" className="sr-only">Email</Label>
        <Control.text model=".email" id="email" name="email" placeholder="Email" className="form-control" defaultValue={email} required autoFocus
        validators={{
          required, validEmail
        }} />
        <Label htmlFor="password" className="sr-only">Password</Label>
        <Control.password model=".password" id="password" name="password" placeholder="Password" className="form-control" defaultValue={password} required />
        <Errors className="text-danger" model=".email" show="touched"
        messages={{
          required: 'Required',
          validEmail: 'Email format invalid'
        }} />
        <div className="checkbox mb-3">
          <Label>
            <Control.checkbox model=".remember" id="remember" name="remember" /> Remember Me
          </Label>
        </div>
        <Button className="btn btn-lg btn-block" color="primary" type="submit">Sign in</Button>
        <p className="mt-5 mb-3 text-muted">&copy; {new Date().getFullYear()}</p>
      </LocalForm>
    </div></div>
  );
}

export default Login;