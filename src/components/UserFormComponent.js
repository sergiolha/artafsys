import React from 'react';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Button, FormGroup, Label } from 'reactstrap';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);
const validEmail = (val) => /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/i.test(val);

/**
 * UserForm component class
 * 
 * The component handles the form for users
 */
class UserForm extends React.Component {

  render() {
    return(
      <div>
        <LocalForm onSubmit={(values) => this.props.handleSubmit(values, this.props.user? this.props.user.id : null)}>
          <FormGroup>
            <Label htmlFor="profile">Profile</Label>
            <Control.select model=".profile" name="profile" id="profile" className="form-control" defaultValue={this.props.user ? this.props.user.profileId : this.props.profiles[0].id.toString()}>
              {this.props.profiles.map((profile) => <option key={profile.id} value={profile.id}>{profile.name}</option>)}
            </Control.select>
          </FormGroup>
          <FormGroup>
            <Label htmlFor="name">Name</Label>
            <Control.text model=".name" id="name" name="name" placeholder="Name" className="form-control" defaultValue={this.props.user ? this.props.user.name : ''}
              validators={{
                required, minLength: minLength(3), maxLength: maxLength(25)
              }} />
              <Errors className="text-danger" model=".name" show="touched"
              messages={{
                required: 'Required',
                minLength: 'Must be greater than 2 characters',
                maxLength: 'Must be 25 characters or less'
              }} />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="email">Email</Label>
            <Control.text model=".email" id="email" name="email" placeholder="Email" className="form-control" defaultValue={this.props.user ? this.props.user.email : ''}
              validators={{
                required, validEmail
              }} />
              <Errors className="text-danger" model=".email" show="touched"
              messages={{
                required: 'Required',
                validEmail: 'Invalid email format'
              }} />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="password">Password</Label>
            <Control.password model=".password" id="password" name="password" placeholder="Password" className="form-control"
              validators={{
                required, minLength: minLength(8), maxLength: maxLength(16)
              }} />
              <Errors className="text-danger" model=".password" show="touched"
              messages={{
                required: 'Required',
                minLength: 'Must be greater than 7 characters',
                maxLength: 'Must be 16 characters or less'
              }} />
          </FormGroup>
          <Button type="submit" value="submit" color="primary">Submit</Button>
        </LocalForm>
      </div>
    );
  }
}

export default UserForm;