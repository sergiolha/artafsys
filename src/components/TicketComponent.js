import React from 'react';
import { Link } from 'react-router-dom';
import { Loading } from './LoadingComponent';
import TicketForm from './TicketFormComponent';
import { Button, Modal, ModalHeader, ModalBody } from 'reactstrap';
import { AuthContext } from '../context/auth';

/**
 * Tickets component class
 * 
 * The component handles listing, adding, editing and deleting of tickets
 */
class Tickets extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isTicketFormOpen: false,
      ticket: null
    }
    this.selectTicket = this.selectTicket.bind(this);
    this.deleteTicket = this.deleteTicket.bind(this);
    this.setStatusTicket = this.setStatusTicket.bind(this);
    this.toggleTicketForm = this.toggleTicketForm.bind(this);
    this.handleAddTicket = this.handleAddTicket.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static contextType = AuthContext;

  selectTicket(ticket) {
    this.setState({ticket: ticket, isTicketFormOpen: true});
  }

  toggleTicketForm() {
    this.setState({isTicketFormOpen: !this.state.isTicketFormOpen});
  }

  handleSubmit(values, id) {
    this.toggleTicketForm();
    if (id === null) {
      this.props.postTicket(this.context.authTokens.userId, 'Pending', values.title, values.description);
    }
    else {
      this.props.patchTicket(id, this.context.authTokens.userId, 'Pending', values.title, values.description);
    }
  }

  handleAddTicket() {
    this.toggleTicketForm();
    this.selectTicket(null);
  }

  deleteTicket(ticket) {
    var res = window.confirm('¿Are you sure?');
    if (res) { this.props.delTicket(ticket.id); }
  }

  setStatusTicket(ticket, status) {
    var res = window.confirm('¿Are you sure?');
    if (res) { this.props.patchTicket(ticket.id, ticket.userId, status, ticket.title, ticket.description); }
  }

  render() {
    const currentUser = this.context.authTokens;
    if (this.props.tickets.isLoading) {
      return(<Loading />);
    }
    else if (this.props.tickets.errMess) {
      return(<h4>{this.props.tickets.errMess}</h4>);
    }
    else {
      var tickets = this.props.tickets.data;
      if (currentUser.profileId === 3) {
        tickets = tickets.filter((t) => t.userId === currentUser.userId);
      }
      return(
        <div>
          {currentUser.profileId === 3 ?
          <Button type="button" className="btn-primary" onClick={this.handleAddTicket}>
            <span className="fa fa-pencil fa-lg"></span> Add Ticket
          </Button>
          : null }
          <br/>
          <br/>
          <div className="table-responsive">
            <table className="table table-striped table-sm">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Status</th>
                  {currentUser.profileId === 2 ? <th>Actions</th> : null }
                </tr>
              </thead>
              <tbody>
                {tickets.map((ticket) => {
                  return (
                    <tr key={ticket.id}>
                        <td><Link to={`/dashboard/tickets/${ticket.id}`}>{ticket.id}</Link></td>
                        <td>{ticket.title}</td>
                        <td>{ticket.status}</td>
                        {currentUser.profileId === 2 ?
                        <td>
                            { ticket.status === 'Pending' ? <button className="btn btn-primary" onClick={() => {this.setStatusTicket(ticket, 'Approved')}}>Approve</button> : null }&nbsp;
                            { ticket.status === 'Pending' ? <button className="btn btn-secondary" onClick={() => {this.setStatusTicket(ticket, 'Denied')}}>Deny</button> : null }&nbsp;
                            <button className="btn btn-danger" onClick={() => {this.deleteTicket(ticket)}}>Delete</button>
                        </td>
                            : null
                        }
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
          <Modal isOpen={this.state.isTicketFormOpen} toggle={this.toggleTicketForm}>
            <ModalHeader toggle={this.toggleTicketForm}>{this.state.ticket ? 'Edit Ticket' : 'Add Ticket' }</ModalHeader>
            <ModalBody>
              <TicketForm postTicket={this.props.postTicket} profiles={this.props.profiles} ticket={this.state.ticket} handleSubmit={this.handleSubmit} />
            </ModalBody>
          </Modal>
        </div>
      );
    }
  }
}

export default Tickets;