import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import PrivateRoute from '../PrivateRoute';
import UnauthorizedLayout from '../layouts/UnauthorizedLayout';
import PrimaryLayout from '../layouts/PrimaryLayout';

/**
 * Main component class
 * 
 * The component group routes by access type: public (UnauthorizedLayout) and
 * private (PrimaryLayout). Default route redirect to login page.
 */
class Main extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route path="/auth" component={UnauthorizedLayout} />
          <PrivateRoute path="/dashboard" component={PrimaryLayout} />
          <Redirect to={{pathname:"/auth/login", state:{from: null}}} />
        </Switch>
      </div>
    );
  }
}

export default Main;
