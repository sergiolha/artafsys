import React from 'react';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Button, FormGroup, Label } from 'reactstrap';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);

/**
 * TicketForm component class
 * 
 * The component handles the form for tickets
 */
class TicketForm extends React.Component {

  render() {
    return(
      <div>
        <LocalForm onSubmit={(values) => this.props.handleSubmit(values, this.props.ticket? this.props.ticket.id : null)}>
          <FormGroup>
            <Label htmlFor="title">Name</Label>
            <Control.text model=".title" id="title" name="title" placeholder="Title" className="form-control" defaultValue={this.props.ticket ? this.props.ticket.title : ''}
              validators={{
                required, minLength: minLength(5), maxLength: maxLength(25)
              }} />
              <Errors className="text-danger" model=".title" show="touched"
              messages={{
                required: 'Required',
                minLength: 'Must be greater than 4 characters',
                maxLength: 'Must be 25 characters or less'
              }} />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="description">Description</Label>
            <Control.textarea model=".description" id="description" name="description" placeholder="Description" className="form-control" defaultValue={this.props.ticket ? this.props.ticket.description : ''}
              validators={{
                required, minLength: minLength(10)
              }} />
              <Errors className="text-danger" model=".description" show="touched"
              messages={{
                required: 'Required',
                minLength: 'Must be greater than 9 characters'
              }} />
          </FormGroup>
          <Button type="submit" value="submit" color="primary">Submit</Button>
        </LocalForm>
      </div>
    );
  }
}

export default TicketForm;