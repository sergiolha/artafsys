import React, { useState } from 'react';
import Main from './components/MainComponent';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ConfigureStore } from './redux/configureStore';
import { AuthContext } from './context/auth';

// Configure the global store state
const store = ConfigureStore();

function App(props) {
  // Retrive tokens from localstorage for logged user
  const existingTokens = JSON.parse(localStorage.getItem("tokens"));
  const [authTokens, setAuthTokens] = useState(existingTokens);
  
  // Create tokens in localstorage for logged user
  const setTokens = (data) => {
    localStorage.setItem("tokens", JSON.stringify(data));
    setAuthTokens(data);
  }

  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Provider store={store}>
        <BrowserRouter>
          <Main></Main>
        </BrowserRouter>
      </Provider>
    </AuthContext.Provider>
  );
}

export default App;
