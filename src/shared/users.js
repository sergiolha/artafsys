export const USERS = [
    {
        id: 1,
        email: 'admin@admin.com',
        password: '123456',
        name: 'Administrator',
        profileId: 1
    },
    {
        id: 2,
        email: 'approver@admin.com',
        password: '123456',
        name: 'John Doe',
        profileId: 2
    },
    {
        id: 3,
        email: 'user@admin.com',
        password: '123456',
        name: 'Karl Marx',
        profileId: 3
    }
];