import * as ActyonTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';

// Users
export const addUser = (user) => ({
    type: ActyonTypes.ADD_USER,
    payload: user
});

export const updUser = (user) => ({
    type: ActyonTypes.UPD_USER,
    payload: user
});

export const deleteUser = (user) => ({
    type: ActyonTypes.DEL_USER,
    payload: user
});

export const postUser = (profileId, name, email, password) => (dispatch) => {

    const newUser = {
        profileId: profileId,
        name: name,
        email: email,
        password: password
    };

    return fetch(baseUrl + 'users', {
        method: "POST",
        body: JSON.stringify(newUser),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                throw error;
            })
        .then(response => response.json())
        .then(response => dispatch(addUser(response)))
        .catch(error => { console.log('post users', error.message); alert('The user could not be saved\nError: ' + error.message); });
};

export const patchUser = (id, profileId, name, email, password) => (dispatch) => {

    const newUser = {
        profileId: profileId,
        name: name,
        email: email,
        password: password
    };

    return fetch(baseUrl + 'users/' + id, {
        method: "PUT",
        body: JSON.stringify(newUser),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                throw error;
            })
        .then(response => response.json())
        .then(response => dispatch(updUser(response)))
        .catch(error => { console.log('post users', error.message); alert('The user could not be updated\nError: ' + error.message); });
};

export const delUser = (id) => (dispatch) => {

    return fetch(baseUrl + 'users/' + id, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
        .then(response => {
            if (response.ok) {
                console.log(response);
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                throw error;
            })
        .then(response => response.json())
        .then(response => dispatch(deleteUser(id)))
        .catch(error => { console.log('post users', error.message); alert('The user could not be deleted\nError: ' + error.message); });
};

export const fecthUsers = () => (dispatch) => {
    dispatch(usersLoading(true));

    return fetch(baseUrl + 'users')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var err = new Error('Error ' + response.status + ': ' + response.statusText);
                err.response = response;
                throw err;
            }
        },
            error => {
                var errm = new Error(error.message);
                throw errm;
            })
        .then(response => response.json(response))
        .then(response => dispatch(addUsers(response)))
        .catch(err => dispatch(usersFailed(err.message)));
}

export const usersLoading = () => ({
    type: ActyonTypes.USERS_LOADING
});

export const usersFailed = (errmess) => ({
    type: ActyonTypes.USERS_FAILED,
    payload: errmess
});

export const addUsers = (users) => ({
    type: ActyonTypes.ADD_USERS,
    payload: users
});

// Tickets
export const addTicket = (ticket) => ({
    type: ActyonTypes.ADD_TICKET,
    payload: ticket
});

export const updTicket = (ticket) => ({
    type: ActyonTypes.UPD_TICKET,
    payload: ticket
});

export const deleteTicket = (ticket) => ({
    type: ActyonTypes.DEL_TICKET,
    payload: ticket
});

export const postTicket = (userId, status, title, description) => (dispatch) => {

    const newTicket = {
        userId: userId,
        status: status,
        title: title,
        description: description
    };

    return fetch(baseUrl + 'tickets', {
        method: "POST",
        body: JSON.stringify(newTicket),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                throw error;
            })
        .then(response => response.json())
        .then(response => dispatch(addTicket(response)))
        .catch(error => { console.log('post tickets', error.message); alert('The ticket could not be saved\nError: ' + error.message); });
};

export const patchTicket = (id, userId, status, title, description) => (dispatch) => {

    const newTicket = {
        userId: userId,
        status: status,
        title: title,
        description: description
    };

    return fetch(baseUrl + 'tickets/' + id, {
        method: "PUT",
        body: JSON.stringify(newTicket),
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                throw error;
            })
        .then(response => response.json())
        .then(response => dispatch(updTicket(response)))
        .catch(error => { console.log('post tickets', error.message); alert('The ticket could not be updated\nError: ' + error.message); });
};

export const delTicket = (id) => (dispatch) => {

    return fetch(baseUrl + 'tickets/' + id, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
        .then(response => {
            if (response.ok) {
                console.log(response);
                return response;
            } else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
            error => {
                throw error;
            })
        .then(response => response.json())
        .then(response => dispatch(deleteTicket(id)))
        .catch(error => { console.log('post tickets', error.message); alert('The ticket could not be deleted\nError: ' + error.message); });
};

export const fecthTickets = () => (dispatch) => {
    dispatch(ticketsLoading(true));

    return fetch(baseUrl + 'tickets')
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var err = new Error('Error ' + response.status + ': ' + response.statusText);
                err.response = response;
                throw err;
            }
        },
            error => {
                var errm = new Error(error.message);
                throw errm;
            })
        .then(response => response.json(response))
        .then(response => dispatch(addTickets(response)))
        .catch(err => dispatch(ticketsFailed(err.message)));
}

export const ticketsLoading = () => ({
    type: ActyonTypes.TICKETS_LOADING
});

export const ticketsFailed = (errmess) => ({
    type: ActyonTypes.TICKETS_FAILED,
    payload: errmess
});

export const addTickets = (tickets) => ({
    type: ActyonTypes.ADD_TICKETS,
    payload: tickets
});