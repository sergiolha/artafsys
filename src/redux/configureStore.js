import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Users } from './users';
import { Profiles } from './profiles';
import { Tickets } from './tickets';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

// Configure storage states
export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            users: Users,
            profiles: Profiles,
            tickets: Tickets
        }),
        applyMiddleware(thunk, logger)
    );

    return store;
}