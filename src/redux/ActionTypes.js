export const ADD_USER = 'ADD_USER';
export const UPD_USER = 'UPD_USER';
export const DEL_USER = 'DEL_USER';
export const USERS_LOADING = 'USERS_LOADING';
export const USERS_FAILED = 'USERS_FAILED';
export const ADD_USERS = 'ADD_USERS';

export const ADD_TICKET = 'ADD_TICKET';
export const UPD_TICKET = 'UPD_TICKET';
export const DEL_TICKET = 'DEL_TICKET';
export const TICKETS_LOADING = 'TICKETS_LOADING';
export const TICKETS_FAILED = 'TICKETS_FAILED';
export const ADD_TICKETS = 'ADD_TICKETS';