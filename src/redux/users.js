import * as ActyonTypes from './ActionTypes';

export const Users = (state = {isLoading: true, errMess: null, data: []}, action) => {
    var user;
    var users;
    switch (action.type) {
        case ActyonTypes.ADD_USER:
            user = action.payload;
            user.id = state.data.length + 1;
            return {...state, isLoading: false, errMess: null, data: state.data.concat(user)};
        case ActyonTypes.ADD_USERS:
            return {...state, isLoading: false, errMess: null, data: action.payload};
        case ActyonTypes.USERS_LOADING:
            return {...state, isLoading: true, errMess: null, data: []};
        case ActyonTypes.USERS_FAILED:
            return {...state, isLoading: false, errMess: action.payload, data: []};
        case ActyonTypes.UPD_USER:
            user = action.payload;
            users = state.data;
            return {...state, isLoading: false, errMess: null, data: users.map((u) => u.id === user.id ? user : u)};
        case ActyonTypes.DEL_USER:
            var id = action.payload;
            users = state.data;
            return {...state, isLoading: false, errMess: null, data: users.filter((u) => u.id !== id)};
        default:
            return state;
    }
}