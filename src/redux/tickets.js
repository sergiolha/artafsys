import * as ActyonTypes from './ActionTypes';

export const Tickets = (state = {isLoading: true, errMess: null, data: []}, action) => {
    var ticket;
    var tickets;
    switch (action.type) {
        case ActyonTypes.ADD_TICKET:
            ticket = action.payload;
            ticket.id = state.data.length + 1;
            return {...state, isLoading: false, errMess: null, data: state.data.concat(ticket)};
        case ActyonTypes.ADD_TICKETS:
            return {...state, isLoading: false, errMess: null, data: action.payload};
        case ActyonTypes.TICKETS_LOADING:
            return {...state, isLoading: true, errMess: null, data: []};
        case ActyonTypes.TICKETS_FAILED:
            return {...state, isLoading: false, errMess: action.payload, data: []};
        case ActyonTypes.UPD_TICKET:
            ticket = action.payload;
            tickets = state.data;
            return {...state, isLoading: false, errMess: null, data: tickets.map((u) => u.id === ticket.id ? ticket : u)};
        case ActyonTypes.DEL_TICKET:
            var id = action.payload;
            tickets = state.data;
            return {...state, isLoading: false, errMess: null, data: tickets.filter((u) => u.id !== id)};
        default:
            return state;
    }
}