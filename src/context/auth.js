import { createContext, useContext } from 'react';

// Context for storing the logged user and using it in components
export const AuthContext = createContext();

export function useAuth() {
  return useContext(AuthContext);
}