import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Header from '../ui/HeaderComponent';
import Menu from '../ui/MenuComponent';
import Dashboard from '../components/DashboardComponent';
import UserLayout from './UserLayout';
import TicketLayout from './TicketLayout';
import PrivateRoute from '../PrivateRoute';

/**
 * PrimaryLayout component
 * 
 * In here, the component handles the private routes. These routes
 * are grouped by modules in a specif component layout. Some routes,
 * require to define "roles" property, because they need authorization
 * for specific roles.
 */
class PrimaryLayout extends React.Component {
  render() {
    return(
      <React.Fragment>
        <Header />
        <div className="container-fluid">
          <div className="row">
            <Menu />
            <main role="main" className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
              <Switch>
                <Route exact path={`${this.props.match.path}`} component={Dashboard} />
                <PrivateRoute path={`${this.props.match.path}/users`} component={UserLayout} roles={[1]} />
                <PrivateRoute path={`${this.props.match.path}/tickets`} component={TicketLayout} roles={[2,3]} />
                <Redirect to={`${this.props.match.url}`} />
              </Switch>
            </main>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default PrimaryLayout;