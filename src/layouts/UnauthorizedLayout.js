import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Login from '../components/LoginComponent';

/**
 * UnauthorizedLayout component
 * 
 * Component used for login feature. If necessary in here, it could handle
 * register, recover password and another public pages.
 * 
 * @param {*} uprops Props from MainComponent
 */
const UnauthorizedLayout = (uprops) => {
  return(
    <div>
      <Switch>
        <Route path="/auth/login" component={Login} />
        <Redirect to="/auth/login" />
      </Switch>
    </div>
  );
}

export default UnauthorizedLayout