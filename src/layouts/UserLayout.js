import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import User from '../components/UserComponent';
import Userdetail from '../components/UserdetailComponent';
import { connect } from 'react-redux';
import { postUser, patchUser, delUser, fecthUsers } from '../redux/ActionCreators';

// Attach storage state to props
const mapStateToProps = state => {
  return {
    profiles: state.profiles,
    users: state.users
  };
}

// Attach actions to props
const mapDispatchToProps = (dispatch) => ({
  postUser: (profileId, name, email, password) => dispatch(postUser(profileId, name, email, password)),
  patchUser: (id, profileId, name, email, password) => dispatch(patchUser(id, profileId, name, email, password)),
  delUser: (id) => dispatch(delUser(id)),
  fecthUsers: () => {dispatch(fecthUsers())}
})

/**
 * UserLayout component
 * 
 * Handle the logic for users like list all users and show
 * information for a specific user.
 */
class UserLayout extends React.Component {
  componentDidMount() {
    this.props.fecthUsers();
  }

  render() {
    // Component to show a specific User
    const UserWithId = ({match}) => {
      let userFound = null;
      let profileFound = null;
      if (this.props.users.data.length > 0) {
        userFound = this.props.users.data.filter((user) => user.id === parseInt(match.params.userId, 10))[0];
        profileFound = this.props.profiles.filter((profile) => profile.id === userFound.profileId)[0];
      }
      return(
        <Userdetail user={userFound} profile={profileFound} isLoading={this.props.users.isLoading} errMess={this.props.users.errMess} />
      );
    };

    return(
      <React.Fragment>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1>Users</h1>
        </div>
        <Switch>
          <Route path={this.props.match.path} exact component={() => <User users={this.props.users} postUser={this.props.postUser} patchUser={this.props.patchUser} delUser={this.props.delUser} profiles={this.props.profiles} />} />
          <Route path={`${this.props.match.path}/:userId`} component={UserWithId} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(UserLayout));