import React from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import Ticket from '../components/TicketComponent';
import Ticketdetail from '../components/TicketdetailComponent';
import { connect } from 'react-redux';
import { postTicket, patchTicket, delTicket, fecthTickets } from '../redux/ActionCreators';

// Attach storage state to props
const mapStateToProps = state => {
  return {
    tickets: state.tickets
  };
}

// Attach actions to props
const mapDispatchToProps = (dispatch) => ({
  postTicket: (userId, status, title, description) => dispatch(postTicket(userId, status, title, description)),
  patchTicket: (id, userId, status, title, description) => dispatch(patchTicket(id, userId, status, title, description)),
  delTicket: (id) => dispatch(delTicket(id)),
  fecthTickets: () => {dispatch(fecthTickets())}
})

/**
 * TicketLayout component
 * 
 * Handle the logic for tickets like list all ticket and show
 * information for a specific ticket.
 */
class TicketLayout extends React.Component {
  componentDidMount() {
    this.props.fecthTickets();
  }

  render() {
    // Component to show a specific Ticket
    const TicketWithId = ({match}) => {
      let ticketFound = null;
      if (this.props.tickets.data.length > 0) {
        ticketFound = this.props.tickets.data.filter((ticket) => ticket.id === parseInt(match.params.ticketId, 10))[0];
      }
      return(
        <Ticketdetail ticket={ticketFound} isLoading={this.props.tickets.isLoading} errMess={this.props.tickets.errMess} />
      );
    };

    return(
      <React.Fragment>
        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
          <h1>Tickets</h1>
        </div>
        <Switch>
          <Route path={this.props.match.path} exact component={() => <Ticket tickets={this.props.tickets} postTicket={this.props.postTicket} patchTicket={this.props.patchTicket} delTicket={this.props.delTicket} />} />
          <Route path={`${this.props.match.path}/:ticketId`} component={TicketWithId} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(TicketLayout));